import { Typography } from "@material-ui/core";
import * as React from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import App from "./App";
import configureStore from "./configureStore";
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import firebase from "firebase";

const { persistor, store } = configureStore();

// react-redux-firebase config
const rrfConfig = {
  // userProfile: 'users'
  // useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
};

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  // createFirestoreInstance // <- needed if using firestore
};

export function ReduxRoot() {
	return (
		<Provider store={store}>
      <ReactReduxFirebaseProvider {...rrfProps}>
				<PersistGate
					loading={<Typography>Loading...</Typography>}
					persistor={persistor}
				>
					<App />
				</PersistGate>
			</ReactReduxFirebaseProvider>
		</Provider>
	);
}
