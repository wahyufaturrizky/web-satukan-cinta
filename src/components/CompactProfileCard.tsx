import React from 'react';
import { makeStyles, createStyles, CardMedia, Card, Typography, CardContent } from '@material-ui/core';
import clsx from 'clsx';
import { useSquareCardMediaStyles } from '@mui-treasury/styles/cardMedia/square';
import { useText01CardContentStyles } from '@mui-treasury/styles/cardContent/text01';
import { useBouncyShadowStyles } from '@mui-treasury/styles/shadow/bouncy';

const useStyles = makeStyles(theme => createStyles({
	// CompactProfileCard
  cardRoot: {
		width: '100%',
		height: '100%',
    margin: 'auto',
    boxShadow: 'none',
    borderRadius: 0,
  },
  cardDetails: {
    display: 'flex',
    flexDirection: 'column',
  },
  cardContent: {
		padding: 8,
		'&:last-child': {
			padding: 8,
		},
  },
}));

export function CompactProfileCard(props: any) {
	const classes = useStyles();
	const mediaStyles = useSquareCardMediaStyles();
	const textCardContentStyles = useText01CardContentStyles();
	const shadowStyles = useBouncyShadowStyles();
	const { compactProfile } = props;
	return (
    <Card className={clsx(classes.cardRoot, shadowStyles.root)}>
      {compactProfile.photoThumbnailUrl && <CardMedia
        classes={mediaStyles}
        image={compactProfile.photoThumbnailUrl}
				title={compactProfile.slug}
      />}
			{!compactProfile.photoThumbnailUrl && <CardMedia
				classes={mediaStyles}
				title={compactProfile.slug}
				style={{backgroundColor: 'silver'}}
			>&nbsp;</CardMedia>}
			<CardContent className={classes.cardContent}>
				<Typography gutterBottom variant="body2" component="h5">
				<strong>{compactProfile.slug}</strong> {compactProfile.age}
				</Typography>
				<Typography variant="caption" color="textSecondary" component="p">
				{compactProfile.addressCityDisplayName}
				</Typography>

				{/* <TextInfoCardContent
					classes={textCardContentStyles}
					heading={compactProfile.slug + ' - ' + compactProfile.age}
					body={compactProfile.addressCityDisplayName} /> */}
			</CardContent>
		</Card>
	);
}
