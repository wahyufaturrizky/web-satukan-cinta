import { Button, Typography, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Card, CardContent, IconButton, CardMedia, GridList, Grid, Container, CircularProgress } from "@material-ui/core";
import React from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
// import { useHistory } from 'react-router-dom'; // if you use react-router
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useParams, generatePath } from "react-router-dom";
import { Waypoint } from 'react-waypoint';
import { CompactProfilesPanel } from 'components/CompactProfilesPanel';
import { FraudWarningPanel } from 'components/FraudWarningPanel';

const useStyles = makeStyles(theme => createStyles({
	root: {
		height: "100%",
		// textAlign: "center",
		paddingTop: 20,
		paddingLeft: 15,
		paddingRight: 15,
		overflowY: 'auto',
	},

	centerContainer: {
		flex: 1,
		height: "90%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "column",
	},

	button: {
		marginTop: 20,
	},
	
}));

export function CompactProfileListPage() {
	const classes = useStyles();
	const { after, first, before, last, gender, religionFamily, relationships, minAge, maxAge } = useParams();

	// const firebase = useFirebase();
	// const auth = useSelector((state: any) => state.firebase.auth);
	console.debug('after=', after, 'first=', first, 'before=', before, 'last=', last);
	const query = useQuery(gql`
	query($after: String, $first: Int, $before: String, $last: Int,
		$gender: Gender, $religionFamily: Religion, $relationships: [Relationship!], $minAge: Int, $maxAge: Int) {
		compactProfiles(sortProperty: "ctr", sortDirection: DESC,
			after: $after, first: $first, before: $before, last: $last,
			matchProfileStatus: A,
			gender: $gender, religionFamily: $religionFamily, relationships: $relationships, minAge: $minAge, maxAge: $maxAge) { 
			edges {
				cursor
				node {
					slug, age, addressCityDisplayName, photoV3, photoThumbnailUrl
				}
			}
			pageInfo {
				hasNextPage, hasPreviousPage, startCursor, endCursor
			}
		}
	}`, {
		variables: { after, first, before, last,
			gender, religionFamily, relationships, minAge, maxAge }
	});
	const MORE_COMPACT_PROFILES_QUERY = gql`
		query($after: String, $first: String, $before: String, $last: String,
			$gender: Gender, $religionFamily: Religion, $relationships: [Relationship!], $minAge: Int, $maxAge: Int) {
			compactProfiles(sortProperty: "ctr", sortDirection: DESC,
				after: $after, first: $first, before: $before, last: $last,
				matchProfileStatus: A,
				gender: $gender, religionFamily: $religionFamily, relationships: $relationships, minAge: $minAge, maxAge: $maxAge) { 
				edges {
					cursor
					node {
						slug, age, addressCityDisplayName, photoV3, photoThumbnailUrl
					}
				}
				pageInfo {
					hasNextPage, endCursor
				}
			}
		}`;
	const nextAfter = query.data && query.data.compactProfiles.pageInfo.hasNextPage && query.data.compactProfiles.pageInfo.endCursor;
	const nextPath = nextAfter && generatePath(CompactProfileListPage.path, {
		after: nextAfter,
		gender, religionFamily, relationships, minAge, maxAge });

	const loadNextPage = async () => {
		if (query.loading) {
			console.info('still loading, not querying more');
			return;
		}
		if (!query.error && !query.data.compactProfiles.pageInfo.hasNextPage) {
			console.info('no next page, not querying more');
			return;
		}
		console.info('querying more');
		query.fetchMore({
			variables: {
				after: nextAfter,
				gender, religionFamily, relationships, minAge, maxAge },
			updateQuery: (previousResult: any, { fetchMoreResult }: any) => {
				const newEdges = fetchMoreResult.compactProfiles.edges;
				const pageInfo = fetchMoreResult.compactProfiles.pageInfo;
				return newEdges.length > 0
					? {
						compactProfiles: {
							__typename: previousResult.compactProfiles.__typename,
							edges: [...previousResult.compactProfiles.edges, ...newEdges],
							pageInfo
						}
					}
					: previousResult;
			}
		})
	};
	return (
		<Container className={classes.root}>

			Profiles

			<FraudWarningPanel />

			<CompactProfilesPanel style={{marginBottom: 24}} query={query}/>

			{query.loading && <CircularProgress />}

			{!query.loading && query.data && query.data.compactProfiles &&
				<Waypoint scrollableAncestor={window} onEnter={loadNextPage}>
					<div style={{height: 24}}></div>
				</Waypoint>}
		</Container>
	);
	// {nextAfter && <Button variant='contained' onClick={loadNextPage}>Selanjutnya</Button>}
}

CompactProfileListPage.path = '/compactProfiles/gender/:gender?/religionFamily/:religionFamily?/relationships/:relationships?/minAge/:minAge?/maxAge/:maxAge?';
