import { History } from "history";
import { combineReducers } from "redux";
import { Todo } from "../model";
import * as todoReducer from "./todo";
import { firebaseReducer } from "react-redux-firebase";
import { persistReducer } from "redux-persist";
import hardSet from "redux-persist/es/stateReconciler/hardSet";
import * as config from 'config.json';
import firebase from "firebase";
// import localStorage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import * as localforage from "localforage";

// ------- Firebase ----------

const firebaseConfig = config.firebaseConfig;

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// export const firebaseAuth = firebase.auth();
// export const facebookProvider = new firebase.auth.FacebookAuthProvider();
// export const googleProvider = new firebase.auth.GoogleAuthProvider();

export interface RootState {
	todoList: Todo[];
}

export default (history: History) =>
	combineReducers({
		firebase: persistReducer(
      { key: 'firebaseState', storage: localforage, stateReconciler: hardSet },
      firebaseReducer
    ),
		...todoReducer,
	});
