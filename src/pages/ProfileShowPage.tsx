import {red} from "@material-ui/core/colors"
import React from "react";
import IntlMessages from "../helpers/IntlMessages";
import { 
	Container, 
	BottomNavigation,
	BottomNavigationAction,
	Card,
	CardHeader,
	Avatar,
	IconButton,
	Button,
	CardActionArea,
	CardMedia,
	CardContent,
	Typography,
	CardActions,
	Box,
	Theme,
	Grid,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Badge,
	AppBar,
	Tabs,
	Tab,
	Divider,
	ListItemAvatar} from "@material-ui/core";
import SwipeableViews from "react-swipeable-views";
import { 
	createStyles, 
	makeStyles,
	useTheme  } from '@material-ui/styles';
import {Home, MoreVert, Wc, Notifications, Message, AccountCircle, AddCircle, Chat, VerifiedUser, Mood, MoodBad, Info, AccessibilityNew, Accessibility, ColorLens, AllInclusive, DirectionsRun, AccountBalanceWallet, KeyboardArrowRight} from '@material-ui/icons';
import BackgroundProfile from '../assets/images/BackgroundProfile.jpg';
import PhotoProfile from '../assets/images/photo-profile.jpg';

import { spacing } from '@material-ui/system';
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { useParams } from "react-router-dom";
// import { useHistory } from 'react-router-dom'; // if you use react-router

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

function a11yProps(index: any) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => createStyles({
	root: {
		height: "100%",
		// textAlign: "center",
	},
	media: {
		height: 0,
		paddingTop: '56.25%', // 16:9
	},
	avatar: {
		backgroundColor: red[500],
	},
	centerContainer: {
		flex: 1,
		height: "90%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "column",
	},
	large: {
		width: theme.spacing(12),
		height: theme.spacing(12),
	},
	button: {
		marginTop: 20,
	},
	title: {
		margin: theme.spacing(4, 0, 2),
	},
	inline: {
      display: 'inline',
    },
	
}));

export function ProfileShowPage() {
	const classes = useStyles();
	const theme = useTheme();
	const [value, setValue] = React.useState(0);

	const {slug} = useParams();

	const query = useQuery(gql`
		query($slug: String)
		{
			user(slug: $slug) {
				id,
				slug,
				name,
				createdAt,
				updatedAt,
				photoV3,
				photoId,
				photoKey,
				photoLocked,
				relationship,
				gender,
				age,
				addressCityDisplayName,
				status,
				matchProfileStatus,
				email,
				religion,
				addressCity,
				addressCountry,
				headline,
				description,
				photoV3,
				photoThumbnailUrl,
				photoFullUrl,
				height,
				heightUnit,
				weightKg,
				skinTone,
				eyewear,
				smoking,
				income,
				preferredRelationship,
				preferredMaxAge,
				preferredMinAge,
				preferredSkinTones,
				preferredMaxHeight,
				preferredCultures,
				preferredMinEduLevelId,
				prefersGlassless,
				prefersHijab,
				preferredMinIncome
			}
		}`, {
		variables: {slug}
	});
	const { loading, error, data } = query;
	console.debug('query:', query);

	const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
	};
	
	const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  
	
	return (
		<Box className={classes.root}>
			{query.loading && <Typography>Loading...</Typography>}
			{query.error && <Typography>Error!</Typography>}
			{!query.loading && query.data && 
				<Box>

					<Card>
						<CardHeader avatar={ <Avatar src={query.data.user.photoThumbnailUrl} aria-label="recipe" className={classes.avatar}>
							
							</Avatar>
							}
							action={
							<IconButton aria-label="settings">
								<MoreVert />
							</IconButton>
							}
							title={query.data.user.slug}
							/>
					</Card>

					<Box style={{position:"relative"}}>
						<Card>
							<CardActionArea>
								<CardMedia className={classes.media} image={BackgroundProfile} title="Background Profile" />
							</CardActionArea>
						</Card>
						<Avatar
							style={{position: "absolute", bottom: -90, left: "50%", right: "50%", transform: "translate(-50%, -50%)"}}
							alt={query.data.user.slug} src={query.data.user.photoThumbnailUrl} className={classes.large} />
					</Box>

					<Box marginTop={8}>
						<Grid container spacing={3}>
							<Grid item xs>
								<List component="nav" aria-label="main mailbox folders">
									<ListItem button>
										<ListItemIcon>
											<Chat />
										</ListItemIcon>
										<ListItemText primary="Message" />
									</ListItem>
								</List>
							</Grid>
							<Grid item xs>
								<List component="nav" aria-label="main mailbox folders">
									<ListItem button>
										<ListItemIcon>
											<VerifiedUser />
										</ListItemIcon>
										<ListItemText primary="Username" />
									</ListItem>
								</List>
							</Grid>
						</Grid>
					</Box>

					<Box marginTop={2}>
						<Grid container style={{justifyContent: "center"}}>
							<Grid item sm>
								<Typography style={{fontWeight: "bold"}}>Status : <span style={{fontWeight: "normal"}}><IntlMessages id={`religion.${query.data.user.religion}`} /> | {query.data.user.addressCity} | {query.data.user.addressCountry}</span></Typography>
							</Grid>
						</Grid>
					</Box>

					<Box marginTop={2}>
						<Grid container style={{justifyContent: "center"}}>
							<Grid item sm>
								<Badge badgeContent={4} color="primary">
									<Mood />
								</Badge>
								<Badge style={{marginLeft: 16}} badgeContent={4} color="secondary">
									<MoodBad />
								</Badge>
								<Badge style={{marginLeft: 16}} badgeContent={4} color="error">
									<Info />
								</Badge>
							</Grid>
						</Grid>
					</Box>

					<Box marginTop={2}>
						<Grid container style={{justifyContent: "center"}}>
							<Grid item sm>
								<Typography variant="h6" align="center">{query.data.user.headline}</Typography>
								<Typography variant="body2" align="center">{query.data.user.description}</Typography>
							</Grid>
						</Grid>
					</Box>

					<Box marginTop={4} marginLeft={2} marginRight={2}>
						<Typography style={{marginBottom: 8}} variant="h6">Gallery</Typography>
						<Grid container>
							<Grid item xs>
								<Card>
									<CardActionArea>
										<CardMedia style={{height: 80, width: 80}} image={query.data.user.photoThumbnailUrl} title="Contemplative Reptile" />
									</CardActionArea>
								</Card>
							</Grid>
							<Grid item xs>
								<Card style={{marginLeft: 8}}>
									<CardActionArea>
										<CardMedia style={{height: 80, width: 80}} image={query.data.user.photoThumbnailUrl} title="Contemplative Reptile" />
									</CardActionArea>
								</Card>
							</Grid>
							<Grid item xs>
								<Card style={{marginLeft: 8}}>
									<CardActionArea>
										<CardMedia style={{height: 80, width: 80}} image={query.data.user.photoThumbnailUrl} title="Contemplative Reptile" />
									</CardActionArea>
								</Card>
							</Grid>
							<Grid item xs>
								<Card style={{marginLeft: 8}}>
									<CardActionArea>
										<CardMedia style={{height: 80, width: 80}} image={query.data.user.photoThumbnailUrl} title="Contemplative Reptile" />
									</CardActionArea>
								</Card>
							</Grid>
						</Grid>
					</Box>

					<Box marginTop={2} marginLeft={2} marginRight={2}>
						<Grid container>
							<Grid item xs>
								<Button fullWidth={true} variant="contained">See all photos</Button>
							</Grid>
						</Grid>
					</Box>

					<Box marginTop={4}>
						<AppBar position="static" color="default">
							<Tabs value={value} onChange={handleChange} indicatorColor="primary" textColor="primary" variant="fullWidth"
								aria-label="full width tabs example">
								<Tab label="Status" {...a11yProps(0)} />
								<Tab label="About Me" {...a11yProps(1)} />
							</Tabs>
						</AppBar>
						<SwipeableViews axis={(theme as any).direction==='rtl' ? 'x-reverse' : 'x' } index={value}
							onChangeIndex={handleChangeIndex}>
							<TabPanel value={value} index={0} dir={(theme as any).direction}>
								<List className={classes.root}>
									<ListItem alignItems="flex-start">
										<ListItemAvatar>
											<Avatar alt="Remy Sharp" src={query.data.user.photoThumbnailUrl} />
										</ListItemAvatar>
										<ListItemText primary={query.data.user.slug} secondary={ <React.Fragment>
											<Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
												20 Feb 2020
											</Typography>
											{" — I'll be in your neighborhood doing errands this READ MORE…"}
											</React.Fragment>
											}
											/>
									</ListItem>
									<Divider variant="inset" component="li" />
									<ListItem alignItems="flex-start">
										<ListItemAvatar>
											<Avatar alt="Travis Howard" src={query.data.user.photoThumbnailUrl} />
										</ListItemAvatar>
										<ListItemText primary={query.data.user.slug} secondary={ <React.Fragment>
											<Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
												20 Feb 2020
											</Typography>
											{" — Wish I could come, but I'm out of town this READ MORE…"}
											</React.Fragment>
											}
											/>
									</ListItem>
									<Divider variant="inset" component="li" />
									<ListItem alignItems="flex-start">
										<ListItemAvatar>
											<Avatar alt={query.data.user.slug} src={query.data.user.photoThumbnailUrl} />
										</ListItemAvatar>
										<ListItemText primary="Oui Oui" secondary={ <React.Fragment>
											<Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
												20 Feb 2020
											</Typography>
											{' — Do you have Paris recommendations? Have you ever READ MORE…'}
											</React.Fragment>
											}
											/>
									</ListItem>
								</List>
							</TabPanel>

							<TabPanel value={value} index={1} dir={(theme as any).direction}>
								<List>
									<Typography>Phisycal</Typography>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<AccessibilityNew />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Tinggi Badan" secondary={`Height: ${query.data.user.height} ${query.data.user.heightUnit}`} />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<AccessibilityNew />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Berat Badan" secondary={`Weight: ${query.data.user.weightKg}`} />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<AccessibilityNew />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Warna kulit" secondary={`Skin Color: ${query.data.user.skinTone}`} />
									</ListItem>
									<Divider style={{marginBottom: 16}} />

									<Typography>Daily</Typography>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<DirectionsRun />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary={`eyewear : ${query.data.user.eyewear}`} />
									</ListItem>
									<Divider style={{marginBottom: 16}} />

									<Typography>Lifestyle</Typography>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<AllInclusive />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary={`Smoker : ${query.data.user.smoking}`} />
									</ListItem>
									<Divider style={{marginBottom: 16}} />

									<Typography>Income</Typography>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<AccountBalanceWallet />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary={`Income : ${query.data.user.income}`} />
									</ListItem>
									<Divider style={{marginBottom: 16}} />

									<Typography>Finding Partner</Typography>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Status" secondary={`Income : ${query.data.user.preferredRelationship}`}  />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Min Age" secondary={`Age : ${query.data.user.preferredMinAge}`} />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Max Age" secondary={`Age : ${query.data.user.preferredMaxAge}`} />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Min Height" secondary={`Height : ${query.data.user.preferredMinHeight} ${query.data.user.heightUnit}`}  />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Max Height" secondary={`Height : ${query.data.user.preferredMaxHeight} ${query.data.user.heightUnit}`}  />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Skin Color" secondary={`Skin : ${query.data.user.preferredSkinTones}`}  />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Tribe" secondary={`Culture : ${query.data.user.preferredCultures}`}  />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Minimal Education" secondary={`Min. Edu : ${query.data.user.preferredMinEduLevelId}`}/>
									</ListItem>
									
									{query.data.user.gender == "M" && <ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Daily" secondary={`Berhijab : ${query.data.user.prefersHijab}`}  />
									</ListItem>}
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Life style" secondary="Lorem ipsum" />
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<KeyboardArrowRight />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Minimal Income" secondary={`min. income : ${query.data.user.preferredMinIncome}`}  />
									</ListItem>
									<Divider style={{marginBottom: 16}} />

									<Typography>Coude Couple</Typography>
									<ListItem>
										<ListItemAvatar>
											<Avatar>
												<Wc />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary="Lorem ipsum" />
									</ListItem>
								</List>
							</TabPanel>
						</SwipeableViews>
					</Box>

					<BottomNavigation style={{alignItems: "flex-end"}} value={value} onChange={(event, newValue)=> {
						setValue(newValue);
						}}
						showLabels
						>
						<BottomNavigationAction value="recents" icon={<Home />} />
						<BottomNavigationAction value="favorites" icon={<Notifications />} />
						<BottomNavigationAction value="nearby" icon={<AddCircle />} />
						<BottomNavigationAction value="folder" icon={<Message />} />
						<BottomNavigationAction value="folder" icon={<AccountCircle />} />
					</BottomNavigation>

				</Box>}
		</Box>
	);
}

ProfileShowPage.path = '/u/:slug';