import React from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import { ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => createStyles({
  warningHeading: {
    fontSize: (theme as any).typography.pxToRem(15),
		fontWeight: 'bold',
		color: 'orange'
	},
}));

export function FraudWarningPanel() {
  const classes = useStyles();

  return (
		<ExpansionPanel style={{marginBottom: 24}}>
			<ExpansionPanelSummary
				expandIcon={<ExpandMoreIcon />}
				aria-controls="panel1a-content"
				id="panel1a-header"
			>
				<Typography className={classes.warningHeading}>⚠ Waspada Penipuan</Typography>
			</ExpansionPanelSummary>
			<ExpansionPanelDetails>
				<Typography>
					Jangan mentransfer uang dengan alasan apa pun ke anggota Heartenly yang baru Anda kenal.
					Segera email admin di <a href="mailto:info@heartenly.com" target="_blank">info@heartenly.com</a> bila ada anggota yang berniat tidak baik.
				</Typography>
			</ExpansionPanelDetails>
		</ExpansionPanel>
  );
}
