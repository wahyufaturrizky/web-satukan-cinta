declare module 'jss-preset-default';
declare module 'react-jss/*';

declare module '@mui-treasury/components/*';
declare module '@mui-treasury/styles/*';
