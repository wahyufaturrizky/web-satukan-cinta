// prettier-ignore
import { AppBar, Badge, Divider, Drawer as DrawerMui, Hidden, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography, useMediaQuery } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles";
import FormatListNumberedIcon from "@material-ui/icons/FormatListNumbered";
import HomeIcon from "@material-ui/icons/Home";
import ChatIcon from '@material-ui/icons/Chat';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import EditIcon from '@material-ui/icons/Edit';
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";
import SettingsIcon from "@material-ui/icons/Settings";
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import MenuIcon from "@material-ui/icons/Menu";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useSelector } from "react-redux";
import { Route, Router, useHistory, useLocation } from "react-router-dom";
import { history } from "./configureStore";
import { Todo } from "./model";
import { HomePage, TodoPage, LoginPage, CompactProfileListPage } from "./pages";
import { RootState } from "./reducers/index";
import { withApollo } from './withApollo';
import { withRoot } from "./withRoot";
import { useFirebase, isLoaded, isEmpty } from 'react-redux-firebase';
import { useEffect } from "react";
import { ProfileShowPage } from "pages/ProfileShowPage";

function Routes() {
	const classes = useStyles();

	return (
		<div className={classes.content}>
			<Route exact={true} path="/" component={LoginPage} />
			<Route exact={true} path="/home" component={HomePage} />
			<Route exact={true} path={CompactProfileListPage.path} component={CompactProfileListPage} />
			<Route exact={true} path="/u/:slug" component={ProfileShowPage} />

			<Route exact={true} path="/todo" component={TodoPage} />
		</div>
	);
}

function Drawer(props: { todoList: Todo[] }) {
	const auth = useSelector((state: any) => state.firebase.auth);
	const firebase = useFirebase();
	const classes = useStyles();
	const history = useHistory();

	async function signOut() {
		await firebase.logout();
		history.push('/');
	}

	if (!isLoaded(auth)) {
		return null;
	}
	if (isEmpty(auth)) {
		return (
			<div>
				<div className={classes.drawerHeader} />
				<Divider />
				<List>
					<ListItem button onClick={() => history.push("/")}>
						<ListItemIcon>
							<HomeIcon />
						</ListItemIcon>
						<ListItemText primary="Sign In" />
					</ListItem>
				</List>
			</div>
		);
	} else {
		return (
			<div>
				<div className={classes.drawerHeader} />
				<Divider />
				<List>
					<ListItem button onClick={() => history.push("/home")}>
						<ListItemIcon>
							<HomeIcon />
						</ListItemIcon>
						<ListItemText primary="Home" />
					</ListItem>
					<ListItem button onClick={() => history.push("/chat")}>
						<ListItemIcon>
							<ChatIcon />
						</ListItemIcon>
						<ListItemText primary="Chat" />
					</ListItem>
					<ListItem button onClick={() => history.push("/profile/me")}>
						<ListItemIcon>
							<AccountBoxIcon />
						</ListItemIcon>
						<ListItemText primary="View Profile" />
					</ListItem>
					<ListItem button onClick={() => history.push("/profile/edit")}>
						<ListItemIcon>
							<EditIcon />
						</ListItemIcon>
						<ListItemText primary="Edit Profile" />
					</ListItem>
					<ListItem button onClick={() => history.push("/profile/changePhoto")}>
						<ListItemIcon>
							<AddAPhotoIcon />
						</ListItemIcon>
						<ListItemText primary="Change Photo" />
					</ListItem>
					<ListItem button onClick={() => history.push("/settings")}>
						<ListItemIcon>
							<SettingsIcon />
						</ListItemIcon>
						<ListItemText primary="Settings" />
					</ListItem>
				</List>
				<Divider />
				<List>
					<ListItem button onClick={() => signOut()}>
						<ListItemIcon>
							<PowerSettingsNewIcon />
						</ListItemIcon>
						<ListItemText primary="Sign Out" />
					</ListItem>
				</List>
				<Divider />
				<List>
					<ListItem button onClick={() => history.push("/todo")}>
						<ListItemIcon>
							<TodoIcon todoList={props.todoList} />
						</ListItemIcon>
						<ListItemText primary="Todo" />
					</ListItem>
				</List>
			</div>
		);
	}
}

function AppDiv() {
	const classes = useStyles();
	const [mobileOpen, setMobileOpen] = React.useState(false);
	const todoList = useSelector((state: RootState) => state.todoList);
	const isMobile = useMediaQuery((theme: Theme) =>
		theme.breakpoints.down("sm")
	);

	const handleDrawerToggle = () => {
		setMobileOpen(!mobileOpen);
	};
	const location = useLocation();
	useEffect(() => {
		console.debug('Closing mobile drawer due to location change');
		setMobileOpen(false);
	}, [location]);

	return (
		<div className={classes.root}>
			<div className={classes.appFrame}>
				<AppBar className={classes.appBar}>
					<Toolbar>
						<IconButton
							color="inherit"
							aria-label="open drawer"
							onClick={handleDrawerToggle}
							className={classes.navIconHide}
						>
							<MenuIcon />
						</IconButton>
						<Typography
							variant="h6"
							color="inherit"
							noWrap={isMobile}
						>
							Heartenly
						</Typography>
					</Toolbar>
				</AppBar>
				<Hidden mdUp>
					<DrawerMui
						variant="temporary"
						anchor={"left"}
						open={mobileOpen}
						classes={{
							paper: classes.drawerPaper,
						}}
						onClose={handleDrawerToggle}
						ModalProps={{
							keepMounted: true, // Better open performance on mobile.
						}}
					>
						<Drawer todoList={todoList} />
					</DrawerMui>
				</Hidden>
				<Hidden smDown>
					<DrawerMui
						variant="permanent"
						open
						classes={{
							paper: classes.drawerPaper,
						}}
					>
						<Drawer todoList={todoList} />
					</DrawerMui>
				</Hidden>
				<Routes />
			</div>
		</div>
	);
}

function App() {
	return (
		<Router history={history}>
			<AppDiv />
		</Router>
	);
}

function TodoIcon(props: { todoList: Todo[] }) {
	let uncompletedTodos = props.todoList.filter(t => t.completed === false);

	if (uncompletedTodos.length > 0) {
		return (
			<Badge color="secondary" badgeContent={uncompletedTodos.length}>
				<FormatListNumberedIcon />
			</Badge>
		);
	} else {
		return <FormatListNumberedIcon />;
	}
}

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		width: "100%",
		height: "100%",
		zIndex: 1,
		overflow: "hidden",
	},
	appFrame: {
		position: "relative",
		display: "flex",
		width: "100%",
		height: "100%",
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		position: "absolute",
	},
	navIconHide: {
		[theme.breakpoints.up("md")]: {
			display: "none",
		},
	},
	drawerHeader: { ...theme.mixins.toolbar },
	drawerPaper: {
		width: 250,
		backgroundColor: theme.palette.background.default,
		[theme.breakpoints.up("md")]: {
			width: drawerWidth,
			position: "relative",
			height: "100%",
		},
	},
	content: {
		backgroundColor: theme.palette.background.default,
		width: "100%",
		height: "calc(100% - 56px)",
		marginTop: 56,
		[theme.breakpoints.up("sm")]: {
			height: "calc(100% - 64px)",
			marginTop: 64,
		},
	},
}));

export default withApollo(withRoot(App));
