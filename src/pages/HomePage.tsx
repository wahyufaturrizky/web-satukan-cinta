import { Button, Typography, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Card, CardContent, IconButton, CardMedia, GridList, Grid, Container } from "@material-ui/core";
import { makeStyles, createStyles } from "@material-ui/styles";
import * as React from "react";
import { useSelector } from "react-redux";
import { HomeBox } from "../components";
import { RootState } from "../reducers";
import { useFirebase, isLoaded, isEmpty } from 'react-redux-firebase';
// import { useHistory } from 'react-router-dom'; // if you use react-router
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import clsx from 'clsx';
import ChevronRightRoundedIcon from '@material-ui/icons/ChevronRightRounded';
import TextInfoCardContent from '@mui-treasury/components/cardContent/textInfo';
import { useSquareCardMediaStyles } from '@mui-treasury/styles/cardMedia/square';
import { useText01CardContentStyles } from '@mui-treasury/styles/cardContent/text01';
import { useBouncyShadowStyles } from '@mui-treasury/styles/shadow/bouncy';
import { Link as RouterLink, generatePath } from "react-router-dom";
import { CompactProfileListPage } from "./CompactProfileListPage";
import { CompactProfilesPanel } from "components/CompactProfilesPanel";
import { FraudWarningPanel } from 'components/FraudWarningPanel';

const useStyles = makeStyles(theme => createStyles({
	root: {
		height: "100%",
		// textAlign: "center",
		paddingTop: 20,
		paddingLeft: 15,
		paddingRight: 15,
		overflowY: 'scroll',
	},

	centerContainer: {
		flex: 1,
		height: "90%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "column",
	},

	button: {
		marginTop: 20,
	},

}));

export function HomePage() {
	const classes = useStyles();
	const query = useQuery(gql`
	{
		compactProfiles(sortProperty: "ctr", sortDirection: DESC) { 
			edges {
				cursor
				node {
					slug, age, addressCityDisplayName, photoV3, photoThumbnailUrl
				}
			}
			pageInfo {
				hasNextPage, hasPreviousPage, startCursor, endCursor
			}
		}
	}`);

	// const firebase = useFirebase();
	// const auth = useSelector((state: any) => state.firebase.auth);
	const moreProfilesPath = generatePath(CompactProfileListPage.path, {gender: 'F'})
	return (
		<Container className={classes.root}>

			<FraudWarningPanel />

			<CompactProfilesPanel query={query} style={{marginBottom: 24}} />

			<Button variant='contained' component={RouterLink} to={moreProfilesPath}>
				Profil Lainnya...</Button>

		</Container>
	);
}
