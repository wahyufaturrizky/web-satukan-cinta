import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import * as config from './config.json';

const client = new ApolloClient({
  uri: config.graphqlUri,
});

export function withApollo(Component: any) {
	function WithApollo(props: object) {
		// MuiThemeProvider makes the theme available down the React tree
		// thanks to React context.
		return (
			<ApolloProvider client={client}>
				<Component {...props} />
			</ApolloProvider>
		);
	}

	return WithApollo;
}
