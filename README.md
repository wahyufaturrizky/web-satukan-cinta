# Heartenly PWA

## Setup

Please create `src/config.json` from template `src/config.dev.json`.
Modify the values as appropriate.

## Debugging

Heartenly PWA port for development is `3002`.

1. In Visual Studio Code, install [Chrome debugger extension](https://code.visualstudio.com/docs/nodejs/reactjs-tutorial#_debugging-react)
2. In Debug view, launch "Launch Chrome against localhost:3002"
3. Optional but recommended: Install [React Developer Tools for Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)

## Generate API Clients

1. Export OpenAPI YAML from SwaggerHub as "YAML Unresolved"
2. Install openapi-generator:

        npm install @openapitools/openapi-generator-cli -g

3. Generate:

        openapi-generator generate -c typescript-fetch.json -i D:\tmp\lovia-heartenly-profile-4.0-swagger.yaml -g typescript-fetch -o src/api/profile
        openapi-generator generate -c typescript-fetch.json -i d:\tmp\lovia-geo-1.0-swagger.yaml -g typescript-fetch -o src/api/geo

4. If you catch the namespaces bug: https://github.com/OpenAPITools/openapi-generator/issues/1947
   Please just move the enum into its own `schemas`
5. Unfortunately you need to patch `runtime.ts` `createFetchParams` (before `const body`):

        if (!context.headers['Authorization'] && this.configuration.accessToken) {
            const accessToken = this.configuration.accessToken!("");
            context.headers['Authorization'] = `Bearer ${accessToken}`;
        }

## Deploy to Firebase Hosting

Initially `firebase login` and `firebase init` must be done (this part is already done.) for:

* `stg` environment: Firebase project `heartenly-stg`
* `prd` environment: Firebase project ...

To deploy from local:

    firebase use heartenly-stg
    firebase deploy

To deploy from GitLab CI:

    1. Make sure to protect the `stg` and `prd` branch. 
    2. `firebase login:ci` to get `FIREBASE_TOKEN`
    3. Set `FIREBASE_TOKEN` as CI variables
    4. To deploy `stg` branch: (https://app-stg.heartenly.com/)

        git push origin master:stg

        To deploy `prd` branch: (https://app.heartenly.com/)

        git push origin master:prd


## React info

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3002](http://localhost:3002) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Environment variables used by npm start / npm run build

You can set them using environment variables.

    PORT=3002
    PUB_SERVER_URL=http://localhost:3000
    PUB_STATIC_URL=http://localhost:3000/static
    PWA_SERVER_URL=http://localhost:3002
    PWA_STATIC_URL=http://localhost:3002/static
    MEDIA_URL=https://media.heartenly.com/stg
    PROFILE_API_URL=https://api.heartenly.com/profile/v4stg

More info: https://samaragroup.atlassian.net/browse/HRT-91

## Sass

Note: Jumbo React Docs is outdated. `node-sass` and Ruby/gem sass are not needed. The only one needed is `sass` `devDependencies`.

Change any `.scss` which will be compiled to `src/styles/jumbo.css`.

    npm run sass

## Create React App example with Material-UI, TypeScript, Redux and Routing

This is a new verison with React Hooks, Material-UI 4 and React-Redux 7 (with hooks!). We use this template for all our new projects. If you want to bootstrap a project with the classic approach without hooks but with class components, you are welcome to use the [previous version](https://github.com/innFactory/create-react-app-material-typescript-redux/tree/v1).

<img width="100%" src="screenshot.png" alt="example"/>

Inspired by:

-   [Material-UI](https://github.com/mui-org/material-ui)
-   [react-redux-typescript-boilerplate](https://github.com/rokoroku/react-redux-typescript-boilerplate)

## Contains

-   [x] [Material-UI](https://github.com/mui-org/material-ui)
-   [x] [Typescript](https://www.typescriptlang.org/)
-   [x] [React](https://facebook.github.io/react/)
-   [x] [Redux](https://github.com/reactjs/redux)
-   [x] [Redux-Thunk](https://github.com/gaearon/redux-thunk)
-   [x] [Redux-Persist](https://github.com/rt2zz/redux-persist)
-   [x] [React Router](https://github.com/ReactTraining/react-router)
-   [x] [Redux DevTools Extension](https://github.com/zalmoxisus/redux-devtools-extension)
-   [x] [TodoMVC example](http://todomvc.com)
-   [x] PWA Support

## Roadmap

-   [x] Make function based components and use hooks for state etc.
-   [x] Implement [Material-UIs new styling solution](https://material-ui.com/css-in-js/basics/) based on hooks
-   [x] use react-redux hooks
-   [ ] Hot Reloading -> Waiting for official support of react-scripts

## How to use

Download or clone this repo

```bash
git clone https://github.com/innFactory/create-react-app-material-typescript-redux
cd create-react-app-material-typescript-redux
```

Install it and run:

```bash
npm i
npm start
```

## Enable PWA ServiceWorker [OPTIONAL]

Just comment in the following line in the `index.tsx`:

```javascript
// registerServiceWorker();
```

to

```javascript
registerServiceWorker();
```

## Enable Prettier [OPTIONAL]

1.  Step: Install the Prettier plugin (e.g. the one of Esben Petersen)
2.  Add the following snippet to your settings in VSCode:

```json
    "editor.formatOnSave": true,
    "editor.codeActionsOnSave": {
       "source.organizeImports": true // optional
   },
```

## Enable project snippets [OPTIONAL]

Just install following extension:

<img width="70%" src="vscode_snippet0.png" alt="Project Snippet"/>

After that you can start to type `fcomp` (_for function component_) and you get a template for a new component.

<img width="70%" src="vscode_snippet1.png" alt="Project Snippet"/>
<img width="70%" src="vscode_snippet2.png" alt="Project Snippet"/>

## The idea behind the example

This example demonstrate how you can use [Create React App](https://github.com/facebookincubator/create-react-app) with [TypeScript](https://github.com/Microsoft/TypeScript).

## Contributors

-   [Anton Spöck](https://github.com/spoeck)

Powered by [innFactory](https://innfactory.de/)
