import React from "react";
import { QueryResult } from '@apollo/react-common';
import { Grid } from "@material-ui/core";
import { CompactProfileCard } from "./CompactProfileCard";

export function CompactProfilesPanel(props: {query: QueryResult<any, {}>, style?: React.CSSProperties}) {
	const { loading, error, data } = props.query;
	if (loading) return <p>Loading...</p>;
	if (error) return <p>Error: {error.toString()}</p>;
	
	console.debug('compactProfiles.edges.length=', data.compactProfiles.edges.length,
		'compactProfiles=', data.compactProfiles);
	return ( 
		<Grid container spacing={3} {...props}>
		{data.compactProfiles.edges.map((it: any) => 
			<Grid item key={it.node.slug} xs={6}>
				<CompactProfileCard compactProfile={it.node} />
			</Grid>)}
		</Grid>
	);
}
