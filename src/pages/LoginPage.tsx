import { Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useSelector } from "react-redux";
import { HomeBox } from "../components";
import { RootState } from "../reducers";
import { useFirebase, isLoaded, isEmpty } from 'react-redux-firebase';
// import { useHistory } from 'react-router-dom'; // if you use react-router
// import GoogleButton from 'react-google-button'; // optional
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { useHistory } from "react-router-dom";

export function LoginPage() {
	const classes = useStyles();
	const [boxColor, setBoxColor] = React.useState("red");
	const todoList = useSelector((state: RootState) => state.todoList);

	const firebase = useFirebase();
	const auth = useSelector((state: any) => state.firebase.auth);
	const history = useHistory();

  function loginWithGoogle() {
    return firebase.login({ provider: 'google', type: 'popup' });
  }

	const onButtonClick = () =>
		setBoxColor(boxColor === "red" ? "blue" : "red");
	const signInSuccessUrl = '/home';

	return (
		<div className={classes.root}>
			
			<div>
					<h2>Auth</h2>
					{
						!isLoaded(auth)
						? <span>Loading...</span>
						: isEmpty(auth)
							? 
<StyledFirebaseAuth
				uiConfig={{
					signInFlow: 'popup',
					signInSuccessUrl: signInSuccessUrl,
					signInOptions: [
						(firebase.auth as any).EmailAuthProvider.PROVIDER_ID,
						(firebase.auth as any).GoogleAuthProvider.PROVIDER_ID
					],
					credentialHelper: 'none', // firebaseui.auth.CredentialHelper.NONE,
					callbacks: {
						signInSuccessWithAuthResult: (authResult, redirectUrl) => {
							(firebase as any).handleRedirectResult(authResult).then(() => {
								console.info('Redirecting to...', signInSuccessUrl);
								history.push(signInSuccessUrl); // if you use react router to redirect
							});
							return false;
						},
					},
				}}
				firebaseAuth={firebase.auth()}
					/>

							: <div>
								<button onClick={firebase.logout}>Sign out</button>
								<pre>{JSON.stringify(auth, null, 2)}</pre>
								</div>
					}
			</div>

			<Typography variant="h4" gutterBottom>
				You have {todoList.length} TODOs in your list!
			</Typography>
			<div className={classes.centerContainer}>
				<HomeBox size={300} color={boxColor} />
				<Button
					className={classes.button}
					onClick={onButtonClick}
					variant="outlined"
					color="primary"
				>
					Change Color
				</Button>
			</div>
		</div>
	);
}

const useStyles = makeStyles({
	root: {
		height: "100%",
		textAlign: "center",
		paddingTop: 20,
		paddingLeft: 15,
		paddingRight: 15,
	},

	centerContainer: {
		flex: 1,
		height: "90%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "column",
	},

	button: {
		marginTop: 20,
	},
});
